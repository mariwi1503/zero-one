import { Module } from '@nestjs/common';
import { TopicModule } from './modules/topic/topic.module';
import { NewsModule } from './modules/news/news.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from './config/typeormConfig';

@Module({
  imports: [
    TypeOrmModule.forRoot(typeOrmConfig),
    TopicModule,
    NewsModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
