import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { config } from './config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule)
    , port = config.port;
  app.useGlobalPipes(new ValidationPipe())
  await app.listen(port, () => console.log(`Running on port: ${port}`));
}
bootstrap();
