import { IsNotEmpty, MinLength } from "class-validator";

export class UpdateTopicDto {
    @IsNotEmpty()
    @MinLength(3)
    title: string
}