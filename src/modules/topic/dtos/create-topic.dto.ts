import { IsNotEmpty, MinLength } from "class-validator";

export class CreateTopicDto {
    @IsNotEmpty()
    @MinLength(3)
    title: string
}