import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateTopicDto } from './dtos/create-topic.dto';
import { UpdateTopicDto } from './dtos/update-topic.dto';
import { Topic } from './entities/topic.entity';

@Injectable()
export class TopicService {
    constructor(
        @InjectRepository(Topic)
        private topicRepository: Repository<Topic>
    ) {}

    async getAllTopic() {
        try {
            let result = await this.topicRepository.find()
            result = result.length > 0 ? result : null
            return {
                status: 'success',
                data: result
            }
            
        } catch (error) {
            return {
                status: 'failed',
                message: error.message
            }
        }
    }

    async addNewTopic(createTopicDto: CreateTopicDto) {
        try {
            let topic = this.topicRepository.create(createTopicDto)
            await this.topicRepository.save(topic)
            return {
                status: 'success',
            }
        } catch (error) {
            return {
                status: 'failed',
                message: error.message
            }
        }
    }

    async updateTopic(id: number, updateTopicDto: UpdateTopicDto) {
        try {
            let topic = await this.topicRepository.findOne({where: {id}})
            if(!topic) throw new BadRequestException('Data tidak ditemukan')
            await this.topicRepository.update({id}, {title: updateTopicDto.title})

            return {
                status: 'success',
                id
            }
        } catch (error) {
            return {
                status: 'failed',
                message: error.message
            }
        }
    }

    async deleteTopic(id: number) {
        try {
            let topic = await this.topicRepository.findOne({where: {id}})
            if(!topic) throw new BadRequestException('Data tidak ditemukan')
            await this.topicRepository.delete({ id })

            return {
                status: 'success',
                id
            }
        } catch (error) {
            return {
                status: 'failed',
                message: error.message
            }
        }
    }

    async getTopicsSubNews(id: number) {
        try {
            let query = this.topicRepository.createQueryBuilder('topics')
            query.leftJoinAndSelect('topics.news', 'news')
            query.andWhere('topics.id = :id', {id})

            let result = await query.getOne()
            if(!result) throw new BadRequestException('Data tidak ditemukan')

            return {
                status: 'success',
                data: result
            }
        } catch (error) {
            return {
                status: 'failed',
                message: error.message
            }
        }
    }
}
