import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { CreateTopicDto } from './dtos/create-topic.dto';
import { UpdateTopicDto } from './dtos/update-topic.dto';
import { TopicService } from './topic.service';

@Controller('topic')
export class TopicController {
    constructor(
        private topicService: TopicService
    ) {}

    @Get('list')
    async getAllTopic() {
        return this.topicService.getAllTopic()
    }

    @Post('add')
    async create(@Body() createTopicDto: CreateTopicDto) {
        return this.topicService.addNewTopic(createTopicDto)
    }

    @Get('detail/:id')
    async getById(@Param('id') id: number) {
        return this.topicService.getTopicsSubNews(id)
    }

    @Put('update/:id')
    async update(@Param('id') id: number, @Body() updateTopicDto: UpdateTopicDto) {
        return this.topicService.updateTopic(id, updateTopicDto)
    }

    @Delete('delete/:id')
    async delete(@Param('id') id: number) {
        return this.topicService.deleteTopic(id)
    }

}
