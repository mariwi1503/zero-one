import { Topic } from "src/modules/topic/entities/topic.entity";
import { BaseEntity, Column, CreateDateColumn, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

export enum Status {
    "draft" = "draft",
    "published" = "published",
    "deleted" = "deleted"
}

@Entity()
export class News extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    title: string

    @Column()
    content: string

    @Column({type: 'enum', enum: Status})
    status: Status

    @ManyToMany((type) => Topic, (topics) => topics.news, {
        cascade: ['insert'],
    })
    @JoinTable()
    topics: Topic[]

    @CreateDateColumn()
    created_at: Date

    @UpdateDateColumn()
    updated_at: Date
}