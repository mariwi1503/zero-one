import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { CreateNewsDto } from './dtos/create-news.dto';
import { FilterNewsDto } from './dtos/filter-news.dto';
import { UpdateNewsDto } from './dtos/update-news.dto';
import { NewsService } from './news.service';

@Controller('news')
export class NewsController {
    constructor(
        private readonly newsService: NewsService
    ) {}

    @Get()
    async getNews(@Body() filterNewsDto: FilterNewsDto) {
        return this.newsService.getNews(filterNewsDto)
    }

    @Post('add')
    async createNews(@Body() createNewsDto: CreateNewsDto) {
        return this.newsService.createNews(createNewsDto)
    }

    @Put('update/:id')
    async updateNews(
        @Param('id') id: number,
        @Body() updateNewsDto: UpdateNewsDto
    ) {
        return this.newsService.updateNews(id, updateNewsDto)
    }

    @Delete('delete/:id')
    async deleteNews(@Param('id') id: number) {
        return this.newsService.deleteNews(id)
    }

    @Get(':id')
    async getById(@Param('id') id: number) {
        return this.newsService.getById(id)
    }
}
