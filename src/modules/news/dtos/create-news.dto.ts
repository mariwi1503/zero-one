import { IsArray, IsNotEmpty, IsOptional, IsString, MinLength } from "class-validator";
import { Status } from "../entities/news.entity";

export class CreateNewsDto {
    @IsNotEmpty()
    @IsString()
    @MinLength(3)
    title: string;

    @IsNotEmpty()
    @MinLength(30)
    content: string;

    @IsOptional()
    status: Status;

    @IsOptional()
    @IsArray()
    topic_ids: [];
}