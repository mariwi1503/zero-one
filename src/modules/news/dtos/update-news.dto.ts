import { IsNotEmpty, IsOptional, IsString, MinLength } from "class-validator";
import { Status } from "../entities/news.entity";

export class UpdateNewsDto {
    @IsOptional()
    @IsString()
    @MinLength(3)
    title: string;

    @IsOptional()
    @IsString()
    @MinLength(30)
    content: string;

    @IsOptional()
    status: Status
}