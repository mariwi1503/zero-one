import { IsOptional } from "class-validator";
import { Status } from "../entities/news.entity";

export class FilterNewsDto {
    @IsOptional()
    status: Status

    @IsOptional()
    topic_id: number
}