import { BadRequestException, Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { resolve4 } from 'dns/promises';
import { DataSource, Repository } from 'typeorm';
import { CreateNewsDto } from './dtos/create-news.dto';
import { FilterNewsDto } from './dtos/filter-news.dto';
import { UpdateNewsDto } from './dtos/update-news.dto';
import { News, Status } from './entities/news.entity';

@Injectable()
export class NewsService {
    constructor(
        @InjectRepository(News)
        private newsRepository: Repository<News>,
        private dataSourse: DataSource
    ) {}

    async getNews(filterNewsDto: FilterNewsDto) {
        try {
            let { status, topic_id } = filterNewsDto
            let query = this.newsRepository.createQueryBuilder('news')
            query.leftJoinAndSelect('news.topics', 'topics')

            if(status) {
                query.andWhere('news.status = :status', {status: status.toLocaleLowerCase()})
            }

            if(topic_id) {
                query.andWhere('topics.id = :id', {id: topic_id})
            }

            let result = await query.getMany()
            if(result.length < 1) throw new BadRequestException('Data tidak ditemukan')
            return {
                status: 'success',
                data: result
            }
        } catch (error) {
            return {
                status: 'failed',
                message: error.message
            }
        }
    }

    async getById(id: number) {
        try {
            let query = this.newsRepository.createQueryBuilder('news')
            query.leftJoinAndSelect('news.topics', 'topics')
            query.andWhere('news.id = :id', {id})

            let result = await query.getOne()
            if(!result) throw new BadRequestException('Data tidak ditemukan')
            return {
                status: 'success',
                data: result
            }
        } catch (error) {
            return {
                status: 'failed',
                message: error.message
            }
        }
    }

    async createNews(createNewsDto: CreateNewsDto) {
        try {
            let { title, content, status = Status.draft, topic_ids } = createNewsDto
            let topics = []
            if(topic_ids && topic_ids.length > 0) {
                topic_ids.map((x) => {
                    topics.push({id: x})
                })
            }
            let news = new News()
            news.title = title
            news.content = content
            news.status = status
            news.topics = topics

            await this.dataSourse.manager.save(news)
            return {
                status: 'success'
            }
        } catch (error) {
            return {
                status: 'failed',
                message: error.message
            }
        }
    }

    async updateNews(id: number, updateNewsDto: UpdateNewsDto) {
        try {
            let { title, content, status } = updateNewsDto
            let news = await this.newsRepository.findOne({where: {id}})
            if(!news) throw new BadRequestException('Data tidak ditemukan')

            let new_data = new News()
            if(title) new_data.title = title
            if(content) new_data.content = content
            if(status) new_data.status = status
            await this.newsRepository.update({id}, new_data)

            return {
                status: 'success',
                id
            }
        } catch (error) {
            return {
                status: 'failed',
                message: error.message
            }
        }
    }

    async deleteNews(id: number) {
        try {
            let news = await this.newsRepository.findOne({where: {id}})
            if(!news) throw new BadRequestException('Data tidak ditemukan')

            await this.newsRepository.delete({id})
            return {
                status: 'success',
                id
            }
        } catch (error) {
            return {
                status: 'failed',
                message: error.message
            }
        }
    }
}
