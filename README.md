# Application Name: zero-one

## Installation
```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
# API List

NEWS API

| Routes | EndPoint                            | Description                                            |
| ------ | ----------------------------------- | ------------------------------------------------------ |
| POST   | /news/add                           | add or create news with it's topic(optional)           |
| GET    | /news                               | Get all news, can be filtered by status and topic      |
| GET    | /news/:id                           | Get news detail selected by ID                         |
| PUT    | /news/update/:id                    | Update news with title, content, or status             |
| DELETE | /news/delete/:id                    | Delete single news selected by ID                      |

```
POST
/news/add
title: {string}, it can't be null and must have min 3 character
content: {string}, it can't be null and must have min 25 characters
status: {string}, it's enum (draft, published, deleted) default: draft
topic_ids: {array}, array of related topic_id and it's optional


GET
/news
status: {string}, it's optional
topic_id: {number}, it's optional


GET
/news/:id
id: {number}, params


PUT
/news/update/:id
id: {number}, params
title: {string}, it's optional
content: {string} it's optional
status: {string} it's optional


DELETE
/news/delete/:id
id: {number}, params
```

TOPIC API

| Routes | EndPoint                            | Description                                            |
| ------ | ----------------------------------- | ------------------------------------------------------ |
| POST   | /topic/add                          | add or create new topic                                |
| GET    | /topic/list                         | Get all topic without it's related news                |
| GET    | /topic/detail/:id                   | Get one topic with all it's related news               |
| PUT    | /topic/update/:id                   | Update topic with only title can be updated            |
| DELETE | /topic/delete/:id                   | Delete single topic selected by ID                     |

```
POST
/topic/add
title: {string}, it can't be null and must have min 3 character


GET
/topic/list
doesn't need anything


GET
/topic/detail/:id
id: {number}, params


PUT
/topic/update/:id
id: {number}, params
title: {string}, it can't be null and must have min 3 character


DELETE
/topic/delete/:id
id: {number}, params
```